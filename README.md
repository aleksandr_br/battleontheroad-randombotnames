# BattleOnTheRoad RandomBotNames #

This repo is created to create dummy names for bots in BattleOnTheRoad game

### How To Add Name To List ###

Make sure that you logged in.

* click on BotNames.txt
* Click Edit Btn
* Add your name to the end of the document (from new line)
* Click "Commit" Btn
* Check the "Create a pull request for this change" checkbox
* Click "Commit" btn

After that I can check and approve your changes, and the name will possible appear in your next game with AI :)